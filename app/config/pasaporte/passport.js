//load bcrypt
var bCrypt = require('bcrypt-nodejs');
const uuidv4 = require('uuid/v4');

module.exports = function (passport, cuenta, persona, rol) {
    var Cuenta = cuenta;
    var Persona = persona;
    var Rol = rol;
    var LocalStrategy = require('passport-local').Strategy;
    //serialize
    passport.serializeUser(function (cuenta, done) {
        done(null, cuenta.id);
    });
    // deserialize user 
    passport.deserializeUser(function (id, done) {
        Cuenta.findOne({where: {id: id}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
            if (cuenta) {
                var userinfo = {
                    id: cuenta.id,
                    id_cuenta: cuenta.external_id,
                    id_persona: cuenta.persona.external_id,
                    nombre: cuenta.persona.nombre + " " + cuenta.persona.apellido,
                    rol: cuenta.persona.rol.nombre
                };
                done(null, userinfo);
            } else {
                done(cuenta.errors, null);
            }
        });
    });
    //registro de usuarios por passport
    passport.use('local-signup', new LocalStrategy(
            {
                usernameField: 'cedula',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, cedula, clave, done) {
                var generateHash = function (password) {
                    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
                };
                //verificar si el email no esta registrado
                Cuenta.findOne({
                    where: {
                        cedula: cedula
                    }
                }).then(function (cuenta) {
                    if (cuenta)
                    {
                        return done(null, false, {
                            message: req.flash('cedula_repetida', 'Cuenta existente con esta cedula ' + cedula)
                        });

                    } else
                    {
                        var userPassword = generateHash(clave);
                        Rol.findOne({
                            where: {nombre: 'usuario'}
                        }).then(function (rol) {
                            if (rol) {
                                var dataPersona =
                                        {
                                            nombre: req.body.nombres,
                                            apellido: req.body.apellidos,
                                            email: req.body.correo,
                                            telefono: req.body.fono,
                                            direccion: req.body.direccion,
                                            foto: 'user.ico',
                                            id_rol: rol.id
                                        };
                                Persona.create(dataPersona).then(function (newPersona, created) {
                                    if (!newPersona) {

                                        return done(null, false);
                                    }
                                    if (newPersona) {
                                        console.log("Se ha creado la persona: " + newPersona.id);
                                        var dataCuenta = {
                                            cedula: req.body.cedula,
                                            clave: userPassword,
                                            id_persona: newPersona.id
                                        };
                                        Cuenta.create(dataCuenta).then(function (newCuenta, created) {
                                            if (newCuenta) {
                                                console.log("Se ha creado la cuenta: " + newCuenta.id);
                                                return done(null, newCuenta);
                                            }
                                            if (!newCuenta) {
                                                console.log("cuenta no se pudo crear");
                                                //borrar persona
                                                return done(null, false);
                                            }

                                        });

                                    }
                                });
                            } else {
                                return done(null, false, {
                                    message: 'El rol no existe'
                                });
                                console.log('EL ROL NO EXISTE');
                            }
                        });

                    }
                });
            }
    ));


//inicio de sesion
    passport.use('local-signin', new LocalStrategy(
            {
                // by default, local strategy uses username and password, we will override with email
                usernameField: 'cedula',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, cedula, clave, done) {
                var isValidPassword = function (userpass, password) {
                    return bCrypt.compareSync(password, userpass);
                };
                Cuenta.findOne({where: {cedula: cedula}}).then(function (cuenta) {
                    if (!cuenta) {
                        return done(null, false, {message: req.flash('err_cred', 'Cuenta no existe')});
                    }

                    if (!isValidPassword(cuenta.clave, clave)) {
                        return done(null, false, {message: req.flash('err_cred', 'Clave incorrecta')});
                    }

                    var userinfo = cuenta.get();
                    console.log(cuenta.get());
                    return done(null, userinfo);

                }).catch(function (err) {
                    console.log("Error:", err);
                    return done(null, false, {message: req.flash('err_cred', 'Cuenta erronea')});
                });
            }
    ));

};