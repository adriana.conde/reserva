'use strict';
//variables de modelos
var models = require('../models');
var Cuenta = models.cuenta;
var Persona = models.persona;
var Horario = models.horario;
var Reserva = models.reserva;
var Cancha = models.cancha;
var Rol = models.rol;
var Transferencia = models.transferencia;
var Horario = models.horario;
//fin de variables de modelos
class adminController {
    /**
     * Ver Admin
     * @section Admin
     *  @type  get
     *  @param {solicitud} req 
     *  @url /admin 
     *  @param  {respuesta} res
     */
    verAdmin(req, res) {
        var listaReserva = [];
        //listar todas las reservas para presentar
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            Reserva.findAll({include: [{model: Cancha}, {model: Horario}, {model: Transferencia}, {model: Persona}]}).then(function (reserva) {
                Horario.findAll({}).then(function (horario) {
                    for (var j = 0; j < reserva.length; j++) {
                        var arrayDeCadenas = reserva[j].cant.split(' ');
                        if (arrayDeCadenas.length > 1) {
                            for (var i = 0; i < horario.length; i++) {
                                if (horario[i].id * 1 === arrayDeCadenas[arrayDeCadenas.length - 1] * 1) {
                                    listaReserva.push({
                                        fecha: reserva[j].fecha,
                                        nroCancha: reserva[j].cancha.nroCancha,
                                        horaInicio: reserva[j].horario.horaInicio,
                                        horaFin: horario[i].horaFin,
                                        valorTotal: reserva[j].valorTotal,
                                        nombres: reserva[j].persona.nombre + ' ' + reserva[j].persona.apellido,
                                        foto: reserva[j].persona.foto,
                                        paymentBrand: reserva[j].transferencium.paymentBrand
                                    });//los agrega al json  
                                }
                            }
                        } else {
                            listaReserva.push({
                                fecha: reserva[j].fecha,
                                nroCancha: reserva[j].cancha.nroCancha,
                                horaInicio: reserva[j].horario.horaInicio,
                                horaFin: reserva[j].horario.horaFin,
                                valorTotal: reserva[j].valorTotal,
                                nombres: reserva[j].persona.nombre + ' ' + reserva[j].persona.apellido,
                                foto: reserva[j].persona.foto,
                                paymentBrand: reserva[j].transferencium.paymentBrand
                            });//los agrega al json
                        }
                    }
                    res.render('admin', {titulo: 'Administrador Calva&Calva',
                        partial: 'partials/admin/frm_admin_reservas',
                        nombre: req.user.nombre,
                        rol: req.user.rol,
                        reserva: listaReserva,
                        usuario: persona
                    });
                });
            });
            // fin de listar todas las reservas para presentar
        });
    }
    /**
     * Listar clientes
     * @section Admin
     *  @type  get
     *  @param {solicitud} req  
     *  @url /admin_clientes 
     *  @param  {respuesta} res
     *  
     */
    verCliente(req, res) {
        //listar todas los clientes para presentar
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            Cuenta.findAll({include: {model: Persona, where: {id_rol: 1}}}).then(function (clientes) {
                res.render('admin', {titulo: req.user.nombre,
                    partial: 'partials/admin/frm_admin_clientes',
                    nombre: req.user.nombre,
                    rol: req.user.rol,
                    usuario: persona,
                    cliente: clientes,
                    info: req.flash('info'),
                    error: req.flash('error')
                });
            });
        });
        // fin de listar todas los clientes para presentar
    }
    /**
     * Listar Canchas
     * @section Admin
     *  @type  get
     *  @param {solicitud} req  
     *  @url /admin_cancha 
     *  @param  {respuesta} res
     *  
     */
    verCancha(req, res) {
        //listar todas las canchas para presentar
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            Cancha.findAll({}).then(function (cancha) {
                res.render('admin', {titulo: req.user.nombre,
                    partial: 'partials/admin/frm_admin_cancha',
                    nombre: req.user.nombre,
                    rol: req.user.rol,
                    usuario: persona,
                    cancha: cancha,
                    info: req.flash('info'),
                    error: req.flash('error')
                });
            });
        });
        //fin de listar todas las canchas para presentar
    }
    /**
     * Guardar Cancha
     * @section Admin
     *  @type post
     *  @param {solicitud} req  
     *  @url /admin_cancha_guardar 
     *  @param  {respuesta} res
     *  
     */
    guardarCancha(req, res) {
        if (req.body.external_editar === '0') {
            //guardar canchas canchas 
            Cancha.create({
                nroCancha: req.body.nroCancha,
                precio: req.body.precio
            }).then(function (newCancha, created) {
                if (newCancha) {
                    req.flash('info', 'Se ha creado correctamente');
                    res.redirect('/admin_cancha');
                }
            });
            //fin de guardar canchas 
        } else {
            //actualizar canchas canchas 
            Cancha.update({
                nroCancha: req.body.nroCancha,
                precio: req.body.precio
            }, {where: {external_id: req.body.external_editar}}).then(function (updatedCancha, created) {
                if (updatedCancha) {
                    req.flash('info', 'Se ha modificado correctamente');
                    res.redirect('/admin_cancha');
                }
            });
            // fin de actualizar canchas canchas 
        }
    }
    /**
     * Editar estado de Cancha
     * @section Admin
     *  @type  post
     *  @param {solicitud} req  
     *  @url /admin_cancha_estado 
     *  @param  {respuesta} res
     *  
     */
    estado(req, res) {
        //actualizar estado de cancha 
        Cancha.update({
            estado: req.body.estado_cancha,
            descripcion: req.body.text_descripcion
        }, {where: {external_id: req.body.external_desactivar}}).then(function (updatedCancha, created) {
            if (updatedCancha) {
                if (req.body.estado_cancha === 'true') {
                    req.flash('info', 'Se ha Activado');
                } else {
                    req.flash('info', 'Se ha desactivado');
                }
                res.redirect('/admin_cancha');
            }
        });
        //fin de actualizar estado de cancha 
    }
    /**
     * Ver lista de Horarios
     * @section Admin
     *  @type  get
     *  @param {solicitud} req  
     *  @url /admin_horario 
     *  @param  {respuesta} res
     *  
     */
    verHorario(req, res) {
        //listar todos los horarios  
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            Horario.findAll({}).then(function (horario) {
                res.render('admin', {titulo: req.user.nombre,
                    partial: 'partials/admin/frm_admin_horario',
                    nombre: req.user.nombre,
                    rol: req.user.rol,
                    usuario: persona,
                    horario: horario,
                    info: req.flash('info'),
                    error: req.flash('error')
                });
            });
        });
        //fin de listar todos los horarios
    }
    /**
     * Guardar Horario
     * @section Admin
     *  @type  post
     *  @param {solicitud} req  
     *  @url /admin_horario_guardar 
     *  @param  {respuesta} res
     */
    guardarHorario(req, res) {
        if (req.body.external_editar === '0') {
            //guardar horarios
            Horario.create({
                estado: req.body.ver_uso,
                horaInicio: req.body.horaInico,
                horaFin: req.body.horaFin,
                tipoHorario: req.body.tipo_horario
            }).then(function (newHorario, created) {
                if (newHorario) {
                    req.flash('info', 'Se ha creado correctamente');
                    res.redirect('/admin_horario');
                }
            });
            //fin de guardar horarios
        } else {
            //actualizar horarios
            Horario.update({
                horaInicio: req.body.horaInico,
                horaFin: req.body.horaFin,
                tipoHorario: req.body.tipo_horario,
                estado: req.body.ver_uso
            }, {where: {external_id: req.body.external_editar}}).then(function (updatedHorario, created) {
                if (updatedHorario) {
                    req.flash('info', 'Se ha modificado correctamente');
                    res.redirect('/admin_horario');
                }
            });
            //fin de actualizar horarios
        }
    }
    /**
     * Editar estado del horario
     * @section Admin
     *  @type  post
     *  @param {solicitud} req
     *  @url /admin_horario_guardar 
     *  @param  {respuesta} res
     *  
     */
    esatadoHorario(req, res) {
        //actualizar estado horarios
        if (req.body.external_normal === 'true') {
            Horario.update({
                estado: req.body.external_normal
            }, {where: {tipoHorario: true}}).then(function (updatedHorario, created) {
                if (updatedHorario) {
                    req.flash('info', 'Se ha habilidato horario');
                    res.redirect('/admin_horario');
                }
            });
        } else {
            Horario.update({
                estado: req.body.external_normal
            }, {where: {tipoHorario: true}}).then(function (updatedHorario, created) {
                if (updatedHorario) {
                    req.flash('info', 'Se ha habilidato horario');
                    res.redirect('/admin_horario');
                }
            });
        }

        if (req.body.external_flexible === 'true') {
            Horario.update({
                estado: req.body.external_flexible
            }, {where: {tipoHorario: false}}).then(function (updatedHorario, created) {
                if (updatedHorario) {
                    req.flash('info', 'Se ha habilidato horario');
                    res.redirect('/admin_horario');
                }
            });
        } else {
            Horario.update({
                estado: req.body.external_flexible
            }, {where: {tipoHorario: false}}).then(function (updatedHorario, created) {
                if (updatedHorario) {
                    req.flash('info', 'Se ha habilidato horario');
                    res.redirect('/admin_horario');
                }
            });
        }
        //fin actualizar estado horarios

    }

}

module.exports = adminController;

