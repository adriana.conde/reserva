'use strict';
//variables de modelos
var models = require('../models');
var Cuenta = models.cuenta;
//fin de variables de modelos
class cuentaController {
    /**
     * Guardar Horario
     * @section Admin
     *  @type  post
     *  @param {solicitud} req  
     *  @url /admin_horario_guardar 
     *  @param  {respuesta} res
     */
    registrar(req, res) {
        require('./datos/insert_admin');
        res.render('login', {
            titulo: 'Registrar',
            partial: 'partials/frm_registro',
            error: req.flash("cedula_repetida")
        });
    }
    //mostar vistas de inicio de sesion
    sesion(req, res) {
        require('./datos/insert_admin');
        res.render('login', {
            titulo: 'Iniciar Sesión',
            partial: 'partials/frm_sesion',
            error: req.flash("err_cred")
        });
    }
    //metodo para guardar token
     guardarToken(req, res) {
        var token = req.params.token;
        Cuenta.update({
            token: token
        }, {where: {external_id: req.user.id_cuenta}}).then(function (updatedToken, created) {
            if (updatedToken) {
                console.log(updatedToken);
            }
        });
    }
    //metodo para obtener token de todas las cuentas
    obtenerToken(req, res) {
        Cuenta.findAll({}).then(function (cuenta) {
            res.status(200).json(cuenta);
        });
    }
    //metodo para cerrar session
    cerrar(req, res) {
        req.session.destroy();
        res.redirect("/");
    }
}

module.exports = cuentaController;