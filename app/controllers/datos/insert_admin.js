var bCrypt = require('bcrypt-nodejs');
var models = require('../../models');
var Rol = models.rol;
var Cuenta = models.cuenta;
var Persona = models.persona;
var insert_admin = function () {
    Persona.findOrCreate({where: {id_rol: '2'}, defaults: {
            nombre: 'admin',
            email: 'canchascalvacalva@gmail.com',
            apellido: 'Calva',
            foto: 'user.ico',
            job: '2'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1523', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '1'}, defaults: {
                cedula: '1105279044', 
                clave: clave, 
                job: '1'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del admin");
        });
    });
};
module.exports = insert_admin();