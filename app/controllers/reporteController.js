'use strict';
//variables de modelos
var models = require('../models');
var Persona = models.persona;
var Horario = models.horario;
var Reserva = models.reserva;
var Cancha = models.cancha;
var Rol = models.rol;
var Transferencia = models.transferencia;
//variables para generar pdf
var pdf = require('html-pdf');
let fs = require('fs');
class reporteController {
    generarReporte(req, res) {
        var listaReserva = [];
        var anio = new Date().getFullYear(); //obtener año actual
        var mes = new Date().getMonth() + 1; //obterner mes actual
        var dia = new Date().getDate(); //obtener dia actual
        var fecha_actual = reporteController.formatoFecha(anio, mes, dia);//fromato de la fecha 
        var nombreArchivo = 'reporte-' + fecha_actual + '.pdf';//variable para dar nombre al archivo pdf
        Reserva.findAll({include: [{model: Cancha}, {model: Horario}, {model: Transferencia}, {model: Persona}]}).then(function (reserva) {
            Horario.findAll({}).then(function (horario) {
                //listar todas las reservas 
                for (var j = 0; j < reserva.length; j++) {
                    var arrayDeCadenas = reserva[j].cant.split(' ');
                    if (arrayDeCadenas.length > 1) {
                        for (var i = 0; i < horario.length; i++) {
                            if (horario[i].id * 1 === arrayDeCadenas[arrayDeCadenas.length - 1] * 1) {
                                listaReserva.push({
                                    fecha: reserva[j].fecha,
                                    nroCancha: reserva[j].cancha.nroCancha,
                                    horaInicio: reserva[j].horario.horaInicio,
                                    horaFin: horario[i].horaFin,
                                    valorTotal: reserva[j].valorTotal,
                                    nombres: reserva[j].persona.nombre + ' ' + reserva[j].persona.apellido,
                                    paymentBrand: reserva[j].transferencium.paymentBrand
                                });//los agrega al json  
                            }
                        }
                    } else {
                        listaReserva.push({
                            fecha: reserva[j].fecha,
                            nroCancha: reserva[j].cancha.nroCancha,
                            horaInicio: reserva[j].horario.horaInicio,
                            horaFin: reserva[j].horario.horaFin,
                            valorTotal: reserva[j].valorTotal,
                            nombres: reserva[j].persona.nombre + ' ' + reserva[j].persona.apellido,
                            paymentBrand: reserva[j].transferencium.paymentBrand
                        });//los agrega al json
                    }
                }
                var estiloTabla = '<style>table {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;border-collapse: collapse;width: 100%;font-size:70%;}\n\
                    table td, #customers th {border: 1px solid #ddd;padding: 8px;}table tr:nth-child(even){background-color: #f2f2f2;}\n\
                    table th {padding-top: 12px;padding-bottom: 12px;text-center: left;background-color: #0B8C1B;color: white;}</style>';

                var contenido = estiloTabla + '<div id="pageHeader" style="border-bottom: 1px solid #ddd; padding-bottom: 5px;">\n\
                        <p style="color: #666; margin: 0; padding-top: 12px; padding-bottom: 5px; text-align:right; font-family: sans-serif; font-size: .85em">';
                contenido += fecha_actual + '</p></div><div style="background-color: #fafafa;  margin:1rem;padding:1rem;text-align: center; ">\n\
                        Reservas Realizadas\n\
                        <table>\n\
                        <thead style="text-align: center;">\n\
                          <tr>\n\
                            <th>Nro</th>\n\
                            <th>Nombres</th>\n\
                            <th>Fecha</th>\n\
                            <th>Nro Cancha</th>\n\
                            <th>Hora Inicio</th>\n\
                            <th>Hora Final</th>\n\
                            <th>Valor</th>\n\
                            <th>Tarjeta</th>\n\
                          </tr>\n\
                        </thead>\n\
                        <tbody>';
                var precio = 0;
                //cargar todas las reservas en html pdf
                for (var i = 0; i < listaReserva.length; i++) {
                    contenido += '<tr>\n\
                            <td>' + (i + 1) + '</td>\n\
                            <td>' + listaReserva[i].nombres + '</td>\n\
                            <td>' + listaReserva[i].fecha + '</td>\n\
                            <td>' + listaReserva[i].nroCancha + '</td>\n\
                            <td>' + listaReserva[i].horaInicio + '</td>\n\
                            <td>' + listaReserva[i].horaFin + '</td>\n\
                            <td>$ ' + listaReserva[i].valorTotal + '</td>\n\
                            <td>' + listaReserva[i].paymentBrand + '</td>\n\
                          </tr>';
                    precio += listaReserva[i].valorTotal;
                }
                contenido += '<tr><td></td><td></td><td></td><td></td><td><b>TOTAL:</b></td><td>$ ' + precio + '</td><td></td><tr></tbody></table></div>';
                contenido += '<div id="pageFooter" style="border-top: 1px solid #ddd; padding-top: 5px;">\n\
                        <p style="color: #666; width: 70%; padding-bottom: 5px; text-align: left; font-family: sans-serif; font-size: .65em; float:center;">\n\
                        Esta lista se creó en una computadora y no es válida sin la firma y el sello.</p>\n\
                        <p style="color: #666; margin: 0; padding-bottom: 5px; text-align: right; font-family:sans-serif; font-size: .65em">Página {{page}} de {{pages}}</p></div>';
                var options = {
                    'format': 'A4',
                    'header': {
                        'heigth': '60px'
                    },
                    "footer": {
                        'heigth': '22mm'
                    }
                };
                //crear el pdf en una ruta del proyecto
                pdf.create(contenido, options).toFile('./' + nombreArchivo, function (err, respuesta) {//crreacion del pdf temporal en l acarpeta public reportes
                    if (err) {
                        console.log(err);//mensaje de erro en el caso que lo haya
                    } else {
                        console.log(respuesta);
                        res.download('./' + nombreArchivo, nombreArchivo, function () {//metodo para descargar el pedf
                            fs.unlinkSync('./' + nombreArchivo);// funcion anonima para elminar el reporte del servidor
                        });
                    }
                });
            });
        });
    }
    //metodo para dar formato a la fecha selecionada
    static formatoFecha(anio, mes, dia) {
        var fecha = '';
        if (mes < 10) {
            if (dia < 10) {
                fecha = anio + "-0" + mes + "-0" + dia;
            } else {
                fecha = anio + "-0" + mes + "-" + dia;
            }
        } else {
            if (dia < 10) {
                fecha = anio + "-" + mes + "-0" + dia;
            } else {
                fecha = anio + "-" + mes + "-" + dia;
            }
        }
        return fecha;
    }
}

module.exports = reporteController;