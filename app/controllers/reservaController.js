'use strict';
//pago
var https = require('https');
var querystring = require('querystring');
//email
var mensajeController = require('./mensajeController.js');
//variables de modelos
var models = require('../models');
var Persona = models.persona;
var Horario = models.horario;
var Reserva = models.reserva;
var Cancha = models.cancha;
var Rol = models.rol;
var Transferencia = models.transferencia;

// variables para realizar pago
var checkout;
var jsonRes = '';
//variable de external id de cancha
var Reserva_id = ''; //variable para almacenar el id de la reserva a guardar
var pt = 0; //variable global para almacenar el precio
var idPersona = 0; //variable global para almacenar el id de la persona
var horaFinal = ''; //variable para almacenar la hora final de la reserva
class reservaController {
    /*----------Ver lista de resevas---------------*/
    verReserva(req, res, next) {
        Cancha.findAll({where: {estado: true}}).then(function (cancha) {//busca todas las canchas en la db
            if (req.isAuthenticated()) {
                if (req.user.rol === 'usuario') {
                    //buscar a la persona
                    Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
                        idPersona = persona.id;
                        reservaController.eliminarReserva(persona.id); //borrar reserva no pagada
                        res.render('usuario', {titulo: 'Calva&Calva',
                            partial: 'partials/usuario/frm_reserva',
                            cancha: cancha,
                            nombre: req.user.nombre,
                            rol: req.user.rol,
                            usuario: persona,
                            error_admin: req.flash('error_admin'),
                            error: req.flash('error'),
                            info: req.flash('info')
                        });
                    });
                    //-----fin de buscar persona-----
                    //----------
                } else if (req.user.rol === 'admin') {
                    res.redirect('/admin');
                }
            } else {
                if (idPersona !== 0) {
                    reservaController.eliminarReserva(idPersona);
                }
                res.render('index', {
                    titulo: 'Calva&Calva',
                    partial: 'partials/usuario/frm_reserva',
                    cancha: cancha,
                    info: req.flash('info')
                });
            }
        });
    }
    /*------------Guardar reserva-----------------*/
    guardarReserva(req, res) {
        var pu = req.params.pu;
        pt = req.params.pt;
        var cant = req.params.cant;
        horaFinal = req.body.horaSalida;
        console.log(horaFinal + '...........');
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            //verificar si es una hora
            if (cant !== 'null') {
                //guardar reserva si es una sola hora
                Reserva.create({
                    valorUnitario: pu,
                    valorTotal: pt,
                    fecha: req.body.fechaHiden,
                    cant: cant,
                    id_persona: persona.id,
                    id_horario: req.body.horaEntrada,
                    id_cancha: req.body.cancha
                }).then(function (newReserva, created) {
                    if (newReserva) {
                        Reserva_id = newReserva.id;
                        res.redirect("/ver_pago");
                    }
                });
            } else {
                //guardar reserva si es mayor de una hora
                Reserva.create({
                    valorUnitario: pu,
                    valorTotal: pt,
                    fecha: req.body.fechaHiden,
                    cant: req.body.horaEntrada,
                    id_persona: persona.id,
                    id_horario: req.body.horaEntrada,
                    id_cancha: req.body.cancha
                }).then(function (newReserva, created) {
                    if (newReserva) {
                        Reserva_id = newReserva.id;
                        res.redirect("/ver_pago");
                    }
                });
            }

        });
    }
    //metodo para elminar reservas sin pagar 
    static eliminarReserva(id) {
        Reserva.destroy({where: [{estado: false}, {id_persona: id}]}); //borrar reserva no pagada
    }
    /*---------- VER LA PLANTILLA CON LOS PAGOS---------------*/
    verPago(req, res) {
        // 1. Prepare the checkout
        function request(callback) {
            var path = '/v1/checkouts';
            var data = querystring.stringify({
                'authentication.userId': '8a8294175d602369015d73bf00e5180c',
                'authentication.password': 'dMq5MaTD5r',
                'authentication.entityId': '8a8294175d602369015d73bf009f1808',
                'amount': pt,
                'currency': 'USD',
                'paymentType': 'DB'
            });
            var options = {
                port: 443,
                host: 'test.oppwa.com',
                path: path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': data.length
                }
            };
            var postRequest = https.request(options, function (res) {
                res.setEncoding('utf8');
                res.on('data', function (chunk) {
                    jsonRes = JSON.parse(chunk);
                    return callback(jsonRes);
                });
            });
            postRequest.write(data);
            postRequest.end();
        }
        request(function (responseData) {
            console.log(responseData);
            checkout = responseData.id;
            //renderizar a la vista
            Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
                res.render('usuario', {
                    titulo: 'Calva&Calva',
                    nombre: req.user.nombre,
                    rol: req.user.rol,
                    usuario: persona,
                    partial: 'partials/usuario/frm_pago',
                    info: req.flash('info'),
                    Checkout: checkout,
                    precio: pt
                });

            });
            // finguardar reserva 
        });

    }
    /*----------------------------*/
    /*--RESULTADO DE PAGO Y ACTUALIZACION DE LA TRANSFERENCIA--*/
    verResultadoPago(req, res) {
        function requests(callback) {
            var path = '/v1/checkouts/' + checkout + '/payment';
            path += '?authentication.userId=8a8294175d602369015d73bf00e5180c';
            path += '&authentication.password=dMq5MaTD5r';
            path += '&authentication.entityId=8a8294175d602369015d73bf009f1808';
            var options = {
                port: 443,
                host: 'test.oppwa.com',
                path: path,
                method: 'GET'
            };
            var postRequest = https.request(options, function (res) {
                res.setEncoding('utf8');
                res.on('data', function (chunk) {
                    jsonRes = JSON.parse(chunk);
                    return callback(jsonRes);
                });
            });
            postRequest.end();
        }
        requests(function (responseData) {
            console.log(responseData);
            Transferencia.create({
                paymentBrand: responseData.paymentBrand,
                code: responseData.result.code,
                last4Digits: responseData.card.last4Digits
            }).then(function (newTranferencia, created) {
                Reserva.update({
                    id_transferencia: newTranferencia.id,
                    estado: true
                }, {where: {id: Reserva_id}}).then(function (updateReserva, created) {
                    if (updateReserva) {
                        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
                            Reserva.findOne({where: {id: Reserva_id}, include: [{model: Cancha}, {model: Horario}, {model: Transferencia}, {model: Persona}]}).then(function (reserva) {
                                Persona.findOne({where: {id_rol: '2'}}).then(function (admin) {
                                    Horario.findAll({}).then(function (horario) {
                                        for (var i = 0; i < horario.length; i++) {
                                            if (horario[i].id == horaFinal) {
                                                //envio de mensaje si el usuario tuviera registrado un correo
                                                if (persona.email !== "" && admin.email !== "") {
                                                    var from = 'RESERVAS C&C';
                                                    var asunto = 'Gracias por su reserva ' + req.user.nombre;
                                                    var texto = 'Se realizo reserva con exito para el día ' + reserva.fecha;
                                                    texto += '\n Nro de cancha: ' + reserva.cancha.nroCancha;
                                                    texto += '\n Hora de Inicio: ' + reserva.horario.horaInicio;
                                                    texto += '\n Hora finalización: ' +horario[i].horaFin;
                                                    texto += '\n Valor Unitario: $' + reserva.valorUnitario;
                                                    texto += '\n Valor Total: $' + reserva.valorTotal;
                                                    texto += '\n Recuerde estar 10 minutos antes del partido';
                                                    mensajeController.enviarEmail(from, asunto, texto, admin.email, persona.email);
                                                }
//                           
                                                res.render('usuario', {
                                                    titulo: 'Calva&Calva',
                                                    partial: 'partials/usuario/frm_resultado',
                                                    nombre: req.user.nombre,
                                                    rol: req.user.rol,
                                                    usuario: persona
                                                });
                                            }
                                        }
                                    });
                                    //fin
                                });
                            });
                        });
                    }
                });
            });
        });
    }
    /*VER RESERVAS REALIZADAS DE UN SOLO USUARIO*/
    verReservasHechas(req, res, next) {
        var listaReserva = [];
        Persona.findOne({where: {external_id: req.user.id_persona}}).then(function (persona) {
            Reserva.findAll({where: {id_persona: persona.id}, include: [{model: Cancha}, {model: Horario}, {model: Transferencia}, {model: Persona}]}).then(function (reserva) {
                Horario.findAll({}).then(function (horario) {
                    reservaController.eliminarReserva(persona.id); //borrar reserva no pagada
                    for (var j = 0; j < reserva.length; j++) {
                        var arrayDeCadenas = reserva[j].cant.split(' ');
                        if (arrayDeCadenas.length > 1) {
                            for (var i = 0; i < horario.length; i++) {
                                if (horario[i].id * 1 === arrayDeCadenas[arrayDeCadenas.length - 1] * 1) {
                                    listaReserva.push({
                                        fecha: reserva[j].fecha,
                                        nroCancha: reserva[j].cancha.nroCancha,
                                        horaInicio: reserva[j].horario.horaInicio,
                                        horaFin: horario[i].horaFin,
                                        valorTotal: reserva[j].valorTotal
                                    });//los agrega al json  
                                }
                            }
                        } else {
                            listaReserva.push({
                                fecha: reserva[j].fecha,
                                nroCancha: reserva[j].cancha.nroCancha,
                                horaInicio: reserva[j].horario.horaInicio,
                                horaFin: reserva[j].horario.horaFin,
                                valorTotal: reserva[j].valorTotal
                            });//los agrega al json
                        }
                    }
                    res.render('usuario', {titulo: 'Calva&Calva',
                        partial: 'partials/usuario/frm_actividades',
                        nombre: req.user.nombre,
                        rol: req.user.rol,
                        usuario: persona,
                        reserva: listaReserva,
                        error_admin: req.flash('error_admin'),
                        error: req.flash('error'),
                        info: req.flash('info')
                    });
                });
            });
        });
    }
    
    /*LISTAR HORARIOS*/
    verListaHorario(req, res, next) {
        var HorarioLista = [];
        var horaActual = new Date().getHours(); //obtenr la hora actual
        var fecha_actual = reservaController.formatoFecha(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate());//fromato de la fecha 
        Reserva.findAll({include: {model: Horario}}).then(function (reserva) {//busqueda de reservas realizadas
            Horario.findAll({where: {estado: true}}).then(function (horario) {//busqueda de todos los horarios activos
                var listaHorariosR = reservaController.obtenerHorariosReservados(reserva);
                for (var i = 0; i < horario.length; i++) {
                    var h1 = horario[i].horaInicio;
                    var H1 = h1.substr(0, h1.length - 3) * 1; //obtener la hora 
                    var verificacion = reservaController.verificarHorario(listaHorariosR, horario[i].id, req.params.fecha, req.params.cancha);//llama al metodo verificar horarios en reservas
                    if (verificacion === true) {// lista los horarios disponibles
                        if (horaActual < H1 && fecha_actual === req.params.fecha) {
                            HorarioLista.push({
                                idHorario: horario[i].id,
                                horaInicio: horario[i].horaInicio, 
                                horaFin: horario[i].horaFin
                            });//los agrega al json 
                        }
                        if (fecha_actual !== req.params.fecha) {
                            HorarioLista.push({
                                idHorario: horario[i].id,
                                horaInicio: horario[i].horaInicio, 
                                horaFin: horario[i].horaFin
                            });//los agrega al json
                        }
                    }
                }
                res.status(200).json(HorarioLista);//envia el json para listar en las vistas 
            });
        }).catch(function (err) {
            res.status(500).json(err);//retorna error si existiese
        });
    }

    /*VERIFICAR HORARIOS*/
    static verificarHorario(reserva, horaInicio, fecha, cancha) {
        var ver = true;
        if (reserva.length !== 0) {// ingresa cuando exista al menos una reserva
            for (var i = 0; i < reserva.length; i++) {//recorrer todo el arreglo de reserva
                if (reserva[i].id_horario * 1 === horaInicio * 1 && reserva[i].fecha === fecha && reserva[i].id_cancha * 1 === cancha * 1) {// si existe la hora, la fecha y la cancha en reservas
                    ver = false; //retorna falso
                }
            }
        }
        return ver;
    }
    /*OBTENER TODAS LAS RESERVAS REALIZADAS CON TODOS SUS ID*/
    static obtenerHorariosReservados(reserva) {
        var listaHorariosR = [];
        for (var j = 0; j < reserva.length; j++) {
            var arrayDeCadenas = reserva[j].cant.split(' ');
            if (arrayDeCadenas.length > 1) {
                for (var h = 0; h < arrayDeCadenas.length; h++) {
                    listaHorariosR.push({id_horario: arrayDeCadenas[h], fecha: reserva[j].fecha, id_cancha: reserva[j].id_cancha});//los agrega al json
                }
            } else {
                listaHorariosR.push({id_horario: reserva[j].cant, fecha: reserva[j].fecha, id_cancha: reserva[j].id_cancha});//los agrega al json
            }
        }
        return listaHorariosR;
    }
    /*FORMATO DE LA FECHA SLECIONADA*/
    static formatoFecha(anio, mes, dia) {
        var fecha = '';
        if (mes < 10) {
            if (dia < 10) {
                fecha = anio + "-0" + mes + "-0" + dia;
            } else {
                fecha = anio + "-0" + mes + "-" + dia;
            }
        } else {
            if (dia < 10) {
                fecha = anio + "-" + mes + "-0" + dia;
            } else {
                fecha = anio + "-" + mes + "-" + dia;
            }
        }
        return fecha;
    }
}

module.exports = reservaController;

