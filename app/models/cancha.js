module.exports = function (sequelize, Sequelize) {
    var Cancha = sequelize.define('cancha', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        external_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4
        },
        nroCancha: {
            type: Sequelize.INTEGER
        },
        descripcion:{
            type: Sequelize.TEXT 
        },
        estado: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        precio: {
            type: Sequelize.DOUBLE(7, 2)
        }
    }, {timestamps: false,
        freezeTableName: true});

    Cancha.associate = function (models) {
        models.cancha.hasMany(models.reserva, {
            foreignKey: 'id_cancha'});
    };
    return Cancha;
};