module.exports = function (sequelize, Sequelize) {
    var Horario = sequelize.define('horario', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        horaInicio:{
            type: Sequelize.STRING(10)
        },
        horaFin:{
            type: Sequelize.STRING(10)
        },
        tipoHorario: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        external_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4
        },
        external_normal: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4
        },
        external_flexible: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4
        },
        estado: {
            type: Sequelize.BOOLEAN,
            defaultValue: true

        }
    }, {timestamps: false,
        freezeTableName: true});
    
    Horario.associate= function (models){
        models.horario.hasOne(models.reserva, {
            foreignKey:'id_horario'});
    };
    
    return Horario;
};