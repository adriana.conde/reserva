module.exports = function (sequelize, Sequelize) {
    var persona = require('../models/persona');
    var Persona = new persona(sequelize, Sequelize);
    //horario
    var horario = require('../models/horario');
    var Horario = new horario(sequelize, Sequelize);
    //cancha
    var cancha = require('../models/cancha');
    var Cancha = new cancha(sequelize, Sequelize);
    //transferencia
    var transferencia= require('../models/transferencia');
    var Transferencia= new transferencia(sequelize, Sequelize);
    var Reserva = sequelize.define('reserva', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        valorUnitario: {
            type: Sequelize.DOUBLE(7, 2)
        },
        valorTotal: {
            type: Sequelize.DOUBLE(7, 2)
        },
        cant: {
            type: Sequelize.TEXT
        },
        fecha: {
            type: Sequelize.DATEONLY
        },
        estado: {
            type: Sequelize.BOOLEAN,
            defaultValue: false

        }

    }, {freezeTableName: true,
        createdAt: 'fecha_registro',
        updatedAt: false
    });

    Reserva.belongsTo(Persona, {
        foreignKey: 'id_persona'
    });
    
    Reserva.belongsTo(Horario, {
        foreignKey: 'id_horario',
        constraints: false
    });
    Reserva.belongsTo(Cancha, {
        foreignKey: 'id_cancha',
        constraints: false
    });
    
    Reserva.belongsTo(Transferencia, {
        foreignKey: 'id_transferencia',
        constraints: false
    });

    return Reserva;
};
