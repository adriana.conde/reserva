module.exports = function (sequelize, Sequelize) {
    var Transferencia = sequelize.define('transferencia', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        paymentBrand: {
            type: Sequelize.STRING(50)
        },
        code: {
            type: Sequelize.STRING(50)
        },
        last4Digits: {
            type: Sequelize.STRING(50)
        }
        
    }, {reezeTableName: true,
        createdAt: false,
        updatedAt: false
        
    });

    Transferencia.associate = function (models) {
        models.transferencia.hasOne(models.reserva, {
            foreignKey: 'id_transferencia'});
    };
    return Transferencia;
};