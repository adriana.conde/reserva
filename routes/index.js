var express = require('express');
var router = express.Router();
var passport = require('passport');
//cuenta
var cuenta = require('../app/controllers/cuentaController');
var cuentaController = new cuenta();
//reporte
var reporte = require('../app/controllers/reporteController');
var reporteController = new reporte();
//reserva
var reserva = require('../app/controllers/reservaController');
var reservaController = new reserva();
//persona
var persona = require('../app/controllers/personaController');
var personaController = new persona();
//admin
var admin = require('../app/controllers/adminController');
var adminController = new admin();

//funcion de autentificacion
var auth = function middleWare(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash("err_cred", "Inicia sesion !!!");
        res.redirect('/iniciar_sesion');
    }
};
//funcion de permisos de administrador
var admin = function middleWare(req, res, next) {
    if (req.user.rol === 'admin') {
        next();
    } else {
        req.flash('error_admin', 'Sin Accesso!');
        res.redirect('/');
    }
};

//---------INICIO----------//
/**
 * Vista principal del usaurio
 * @section Inicio
 *  @type  get
*  @param {solicitud} req 
 *  @url /
 *  @param  {respuesta} res
 */
router.get('/', reservaController.verReserva);
/**
 * Vista para iniciar Sesion
 * @section Inicio
 *  @type  get
*  @param {solicitud} req 
 *  @url /iniciar_sesion
 *  @param  {respuesta} res
 */
router.get('/iniciar_sesion', cuentaController.sesion);
/**
 * Vista para regsitrar usaurio
 * @section Inicio
 *  @type  get
*  @param {solicitud} req 
 *  @url /registrar
 *  @param  {respuesta} res
 */
router.get('/registrar', cuentaController.registrar);
/**
 * Registro de usuario
 * @section Inicio
 *  @type  post
*  @param {solicitud} req 
 *  @url /registrar
 *  @param  {respuesta} res
 */
router.post('/registrar', passport.authenticate('local-signup', {
    successRedirect: '/iniciar_sesion',
    failureRedirect: '/registrar'
}));
/**
 * Inicio de Sesión
 * @section Inicio
 *  @type  post
*  @param {solicitud} req 
 *  @url /iniciar_sesion
 *  @param  {respuesta} res
 */
router.post('/iniciar_sesion', passport.authenticate('local-signin', {
    successRedirect: '/',
    failureRedirect: '/iniciar_sesion'
}));
/**
 * Cerrar Sesión
 * @section Inicio
 *  @type  get
*  @param {solicitud} req 
 *  @url /cerrar_sesion
 *  @param  {respuesta} res
 */
router.get('/cerrar_sesion', cuentaController.cerrar);

//--------USUARIO----------//
/**
 * Vista Perfil de actualizacion
 * @section Usuario
 *  @type  get
*  @param {solicitud} req 
 *  @url /user_info
 *  @param  {respuesta} res
 */
router.get('/user_info', auth, personaController.verPersona);
/**
 * Actualizar datos del usuario
 * @section Usuario
 *  @type  post
*  @param {solicitud} req 
 *  @url /user_config
 *  @param  {respuesta} res
 */
router.post('/user_config', auth, personaController.editar);
/**
 * Actualizar foto del usuario
 * @section Usuario
 *  @type  post
*  @param {solicitud} req 
 *  @url /user_subir_imagen
 *  @param  {respuesta} res
 */
router.post('/user_subir_imagen', auth, personaController.guardarFoto);
/**
 * Vista de las reservas realizadas del usuario
 * @section Usuario
 *  @type  get
*  @param {solicitud} req 
 *  @url /ver_reserva
 *  @param  {respuesta} res
 */
router.get('/ver_reserva', auth, reservaController.verReservasHechas);
/**
 * Guardar Token en la cuenta para notificaciones
 * @section Usuario
 *  @type  get
*  @param {solicitud} req 
 *  @url /guardar_token/:token
 *  @param  {respuesta} res
 */
router.get('/guardar_token/:token', auth, cuentaController.guardarToken);
/**
 *ObtenerToken en la cuenta para notificaciones
 * @section Usuario
 *  @type  get
*  @param {solicitud} req 
 *  @url /obtener_token
 *  @param  {respuesta} res
 */
router.get('/obtener_token', auth, cuentaController.obtenerToken);
//------------------//

//--------RESERVA----------//
/**
 * Listar horarios disponibles
 * @section Reserva
 *  @type  get
*  @param {solicitud} req 
 *  @url /listaHorarios/:fecha/:cancha
 *  @param  {respuesta} res
 */
router.get('/listaHorarios/:fecha/:cancha', reservaController.verListaHorario);
/**
 * Guardar reserva
 * @section Reserva
 *  @type  post
*  @param {solicitud} req 
 *  @url /guardar_reserva/:pu/:pt/:cant
 *  @param  {respuesta} res
 */
router.post('/guardar_reserva/:pu/:pt/:cant', auth, reservaController.guardarReserva);
/**
 * Pagar reserva
 * @section Reserva
 *  @type  get
*  @param {solicitud} req 
 *  @url /ver_pago
 *  @param  {respuesta} res
 */
router.get('/ver_pago', auth, reservaController.verPago);
/**
 * Ver respuesta de pago
 * @section Reserva
 *  @type  get
*  @param {solicitud} req 
 *  @url /usuario_pagar_reserva
 *  @param  {respuesta} res
 */
router.get('/usuario_pagar_reserva', auth, reservaController.verResultadoPago);
//------------------//

//--------ADMINISTRADOR----------//
/**
 * Iniciar sesion en administrador con todas las reservas realizadas y sus ingresos
 * @section Administrador
 *  @type  get
 *  @param {solicitud} req 
 *  @url /admin 
 *  @param  {respuesta} res
 */
router.get('/admin', auth, admin, adminController.verAdmin);
/**
 * Listar clientes
 * @section Administrador
 *  @type  get
 *  @param {solicitud} req  
 *  @url /admin_clientes 
 *  @param  {respuesta} res
 *  
 */
router.get('/admin_clientes', auth, admin, adminController.verCliente);
/**
 * Listar Canchas
 * @section Administrador
 *  @type  get
 *  @param {solicitud} req  
 *  @url /admin_cancha 
 *  @param  {respuesta} res
 *  
 */
router.get('/admin_cancha', auth, admin, adminController.verCancha);
/**
 * Guardar Cancha
 * @section Administrador
 *  @type post
 *  @param {solicitud} req  
 *  @url /admin_cancha_guardar 
 *  @param  {respuesta} res
 *  
 */
router.post('/admin_cancha_guardar', auth, admin, adminController.guardarCancha);
/**
 * Editar estado de Cancha
 * @section Administrador
 *  @type  post
 *  @param {solicitud} req  
 *  @url /admin_cancha_estado 
 *  @param  {respuesta} res
 *  
 */
router.post('/admin_cancha_estado', auth, admin, adminController.estado);
/**
 * Ver lista de Horarios
 * @section Administrador
 *  @type  get
 *  @param {solicitud} req  
 *  @url /admin_horario 
 *  @param  {respuesta} res
 *  
 */
router.get('/admin_horario', auth, admin, adminController.verHorario);
/**
 * Guardar Horario
 * @section Administrador
 *  @type  post
 *  @param {solicitud} req  
 *  @url /admin_horario_guardar 
 *  @param  {respuesta} res
 */
router.post('/admin_horario_guardar', auth, admin, adminController.guardarHorario);
/**
 * Editar estado del horario
 * @section Administrador
 *  @type  post
 *  @param {solicitud} req
 *  @url /admin_horario_guardar 
 *  @param  {respuesta} res
 *  
 */
router.post('/admin_horario_estado', auth, admin, adminController.esatadoHorario);

//------------REPORTE------------//
router.get('/reporte', auth, admin, reporteController.generarReporte);


module.exports = router;
